package com.mjcl.loyaltycards;

import android.os.Parcel;
import android.os.Parcelable;

/* Plain java class representing a loyalty card. Contains information about the business
*  and the current progress on the loyalty card. */
public class LoyaltyCard implements Comparable, Parcelable {

    private String id;
    private String name;
    private int points;
    private int logoId;
    private String location;
    private String hours;
    private String facebookUrl;
    private String facebookId;
    private String mapsId;
    private boolean subscribed;

    public LoyaltyCard(String id, String name, int points, int logoId, String location,
                       String hours, String facebookUrl, String facebookId, String mapsId,
                       boolean subscribed) {
        this.id = id;
        this.name = name;
        this.points = points;
        this.logoId = logoId;
        this.location = location;
        this.hours = hours;
        this.facebookUrl = facebookUrl;
        this.facebookId = facebookId;
        this.mapsId = mapsId;
        this.subscribed = subscribed;
    }

    public LoyaltyCard(LoyaltyCard loyaltyCard) {
        this.id = loyaltyCard.getId();
        this.name = loyaltyCard.getName();
        this.points = loyaltyCard.getPoints();
        this.logoId = loyaltyCard.getLogoId();
        this.location = loyaltyCard.getLocation();
        this.hours = loyaltyCard.getHours();
        this.facebookUrl = loyaltyCard.getFacebookUrl();
        this.facebookId = loyaltyCard.getFacebookId();
        this.mapsId = loyaltyCard.getMapsId();
        this.subscribed = loyaltyCard.getSubscribed();
    }


    protected LoyaltyCard(Parcel in) {
        id = in.readString();
        name = in.readString();
        points = in.readInt();
        logoId = in.readInt();
        location = in.readString();
        hours = in.readString();
        facebookUrl = in.readString();
        facebookId = in.readString();
        mapsId = in.readString();
        subscribed = in.readInt() == 1;

    }

    public static final Creator<LoyaltyCard> CREATOR = new Creator<LoyaltyCard>() {
        @Override
        public LoyaltyCard createFromParcel(Parcel in) {
            return new LoyaltyCard(in);
        }

        @Override
        public LoyaltyCard[] newArray(int size) {
            return new LoyaltyCard[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPoints() { return points; }

    public int getLogoId() { return logoId; }

    public String getLocation() { return location; }

    public String getHours() { return hours; }

    public String getFacebookUrl() { return facebookUrl; }

    public String getFacebookId() { return facebookId; }

    public String getMapsId() { return mapsId; }

    public boolean getSubscribed() { return subscribed; }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }

        if(obj == null || getClass() != obj.getClass()) {
            return false;
        }

        LoyaltyCard c = (LoyaltyCard) obj;

        return id == c.getId() &&
                name == c.getName() &&
                points == c.getPoints() &&
                logoId == c.getLogoId() &&
                location == c.getLocation() &&
                hours == c.getHours() &&
                facebookUrl == c.getFacebookUrl() &&
                facebookId == c.getFacebookId() &&
                mapsId == c.getMapsId() &&
                subscribed == c.getSubscribed();
    }

    // returns the value of the cards point after being incremented.
    public int incrementPoints() {

        if(points == 5) {
            points = 1;
        }

        else points++;

        return points;
    }

    public void subscribe(boolean b) {

        subscribed = b;
    }

    @Override
    public int compareTo(Object o) {
        LoyaltyCard compare = (LoyaltyCard) o;

        if(compare.getId().equals(this.id))
        return 0;
        else return 1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeInt(points);
        parcel.writeInt(logoId);
        parcel.writeString(location);
        parcel.writeString(hours);
        parcel.writeString(facebookUrl);
        parcel.writeString(facebookId);
        parcel.writeString(mapsId);
        parcel.writeInt(subscribed? 1 : 0);
    }


}
