package com.mjcl.loyaltycards.utils;

import android.content.pm.PackageManager;

// Helper class for building URLs to use with intents for opening other applications.
public class UrlHelper {

    private UrlHelper() {}

    // https://stackoverflow.com/questions/34564211/
    // Returns the appropriate url depending on the version of facebook installed on the device.
    public static String getFaceBookUrl(PackageManager packageManager, String facebookUrl, String facebookId) {
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.orca", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://page/" + facebookId;
            } else { // normal web url
                return facebookUrl;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return facebookUrl; //normal web url
        }
    }

    public static String getGMapsUrl(String name, String id) {
        String url = "https://www.google.com/maps/search/?api=1&query=";

        String strippedName = name.replaceAll("[!*'();:@&=+$,/\\[\\]?%#]", "");
        String[] queryTerms = strippedName.split(" ");

        for(int i = 0; i < queryTerms.length; i++) {
            if(i == queryTerms.length - 1) {
                url += queryTerms[i] + "&query_place_id=" + id;
            }
            else {
                url += queryTerms[i] + "+";
            }
        }

        return url;

    }
}
