package com.mjcl.loyaltycards.viewmodels;

import android.app.Application;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.mjcl.loyaltycards.LoyaltyCard;
import com.mjcl.loyaltycards.sqlite.DBHelper;

import java.util.ArrayList;
import java.util.List;

/* LoyaltyCardsViewModel maintains current current state of the user's
 * subscribed cafes, their current progress on their loyalty cards.
 * Shared between home and browser views. */
public class LoyaltyCardsViewModel extends AndroidViewModel {

    private DBHelper mDbHelper;
    // subscribed cafes displayed on the main screen.
    private final MutableLiveData<List<LoyaltyCard>> mSubscribedCafes;
    // unsubscribed cafes displayed on the browser screen.
    private final MutableLiveData<List<LoyaltyCard>> mBrowserCafes;

    public LoyaltyCardsViewModel(Application application) {
        super(application);

        mDbHelper = new DBHelper(application);

        mBrowserCafes = new MutableLiveData<>();
        mSubscribedCafes = new MutableLiveData<>();

        loadCafes(DBHelper.SUBSCRIBED, mSubscribedCafes);

        // subscribed cafes stay in the browser list
        loadCafes(DBHelper.ALLCARDS, mBrowserCafes);
    }

    public MutableLiveData<List<LoyaltyCard>> getBrowserCafes() {

        return mBrowserCafes;
    }

    public MutableLiveData<List<LoyaltyCard>> getSubscribedCafes() {

        return mSubscribedCafes;
    }

    // loads either subscribed, unsubscribed or all cafes
    private void loadCafes(int isSubscribed, MutableLiveData<List<LoyaltyCard>> livedata) {

        List<LoyaltyCard> newLoyaltyCards = new ArrayList<>();
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor;

        if(isSubscribed == DBHelper.ALLCARDS) {
            cursor = db.query(DBHelper.TABLENAME_CAFES,
                    new String[]{
                            DBHelper.CAFES_COLUMN_ID,
                            DBHelper.CAFES_COLUMN_CAFENAME,
                            DBHelper.CAFES_COLUMN_USERPOINTS,
                            DBHelper.CAFES_COLUMN_LOGO_ID,
                            DBHelper.CAFES_COLUMN_LOCATION,
                            DBHelper.CAFES_COLUMN_HOURS,
                            DBHelper.CAFES_COLUMN_FB_URL,
                            DBHelper.CAFES_COLUMN_FB_ID,
                            DBHelper.CAFES_COLUMN_MAPS_ID,
                            DBHelper.CAFES_COlUMN_SUBSCRIBED,
                    },
                    null, null, null, null, null);
        } else {
             // Retrieve data from sqlitedb
             cursor = db.query(DBHelper.TABLENAME_CAFES,
                    new String[]{
                            DBHelper.CAFES_COLUMN_ID,
                            DBHelper.CAFES_COLUMN_CAFENAME,
                            DBHelper.CAFES_COLUMN_USERPOINTS,
                            DBHelper.CAFES_COLUMN_LOGO_ID,
                            DBHelper.CAFES_COLUMN_LOCATION,
                            DBHelper.CAFES_COLUMN_HOURS,
                            DBHelper.CAFES_COLUMN_FB_URL,
                            DBHelper.CAFES_COLUMN_FB_ID,
                            DBHelper.CAFES_COLUMN_MAPS_ID,
                            DBHelper.CAFES_COlUMN_SUBSCRIBED,
                    },
                    DBHelper.CAFES_COlUMN_SUBSCRIBED + "=?",
                    new String[]{String.valueOf(isSubscribed)}, null, null, null);
        }

        while(cursor.moveToNext()) {

            String id = cursor.getString(0); // retrieve id

            String name = cursor.getString(1); // retrieve cafe name

            int points = cursor.getInt(2); // retrieve user points

            int logoId = cursor.getInt(3); // retrieve image filename

            String location = cursor.getString(4); // retrieve location

            String hours = cursor.getString(5); // retrieve hours

            String facebookUrl = cursor.getString(6); // retrieve facebook url

            String facebookId = cursor.getString(7); // retrieve facebook id

            String mapsId = cursor.getString(8); // retrive maps id

            boolean subscribed = cursor.getInt(9) == DBHelper.SUBSCRIBED;

            // add to list
            newLoyaltyCards.add(new LoyaltyCard(id, name, points, logoId, location,
                    hours, facebookUrl, facebookId, mapsId, subscribed));
        }

        cursor.close();

        // push to view.
        livedata.setValue(newLoyaltyCards);
    }

    public boolean subscribeToCard(String cardId) {

        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Cursor cursor = db.query(DBHelper.TABLENAME_CAFES,
                new String[] { DBHelper.CAFES_COlUMN_SUBSCRIBED },
                DBHelper.CAFES_COLUMN_ID + "=?", new String[] { String.valueOf(cardId)},
                null, null, null);

        cursor.moveToFirst();
        // card is already subscribed, return false and do nothing else.
        if(cursor.getInt(0) == DBHelper.SUBSCRIBED) return false;

        else {

            // update subscription value in database
            ContentValues cv = new ContentValues();
            cv.put(DBHelper.CAFES_COlUMN_SUBSCRIBED, DBHelper.SUBSCRIBED);
            db.update(DBHelper.TABLENAME_CAFES,
                    cv,
                    DBHelper.CAFES_COLUMN_ID + "=?",
                    new String[]{cardId});
            db.close();


            LoyaltyCard card = null;
            for (LoyaltyCard c : mBrowserCafes.getValue()) {
                if (c.getId().equals(cardId)) {
                    card = new LoyaltyCard(c);
                    card.subscribe(true);
                    break;
                }
            }
            // move cafe from browser list to subscribed list
            // c should never be null.
            if (card != null) {
                addCardToLiveData(mSubscribedCafes, card);
                addCardToLiveData(mBrowserCafes, card);
            }
            return true;
        }
    }

    public void unsubscribeToCard(String cardId) {

        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // update subscription value in db
        ContentValues cv = new ContentValues();
        cv.put(DBHelper.CAFES_COlUMN_SUBSCRIBED, DBHelper.UNSUBSCRIBED);
        db.update(DBHelper.TABLENAME_CAFES,
                cv,
                DBHelper.CAFES_COLUMN_ID + "=?",
                new String[] { cardId });
        db.close();

        LoyaltyCard card = null;
        for(LoyaltyCard c : mSubscribedCafes.getValue())
        {
            if(c.getId().equals(cardId)){
                card = new LoyaltyCard(c);
                card.subscribe(false);
                break;
            }
        }
        // move cafe from browser list to subscribed list.
        // c should never be null.
        if(card != null) {

            addCardToLiveData(mBrowserCafes, card);
            removeCafeFromLiveData(mSubscribedCafes, cardId);
        }

    }

    // helper function for maintaining livedata objects.
    // Clones the list, adds the cafe to the cloned list or updates the cafe if it is already
    // in the list, and sets the cloned list as the new value of the livedata.
    private void addCardToLiveData(MutableLiveData<List<LoyaltyCard>> data, LoyaltyCard c) {

        List<LoyaltyCard> loyaltyCardList = data.getValue();

        ArrayList<LoyaltyCard> clonedList = new ArrayList<>();

        boolean existingCardUpdated = false;

        for(LoyaltyCard loyaltyCard : loyaltyCardList) {
            if(loyaltyCard.getId() == c.getId()) {
                clonedList.add(c);
                existingCardUpdated = true;
            } else {
                clonedList.add(new LoyaltyCard(loyaltyCard));
            }
        }

        if(!existingCardUpdated) clonedList.add(c);

        data.setValue(clonedList);
    }

    // helper function for maintaining livedata objects.
    // Clones the list minus the cafe and sets the cloned
    // list as the new value of the livedata.
    private void removeCafeFromLiveData(MutableLiveData<List<LoyaltyCard>> data, String cardId) {

        List<LoyaltyCard> loyaltyCardList = data.getValue();
        if(loyaltyCardList != null) {
            ArrayList<LoyaltyCard> clonedList = new ArrayList<>();

            for (int i = 0; i < loyaltyCardList.size(); i++) {
                LoyaltyCard c = loyaltyCardList.get(i);
                if (! c.getId().equals(cardId)) {
                    clonedList.add(new LoyaltyCard(c));
                }
            }

            data.setValue(clonedList);
        }
    }

    // Increments the progress of a subscribed loyalty card.
    public LoyaltyCard incrementCard(String cafeId) {

        // Was the card successfully incremented?
        LoyaltyCard result = null;

        // new value to go in database
        int updatedPoints = 0;

        // update the livedata
        List<LoyaltyCard> subscribedLoyaltyCards = mSubscribedCafes.getValue();

        if(subscribedLoyaltyCards != null) {

            ArrayList<LoyaltyCard> clonedList = new ArrayList<>(subscribedLoyaltyCards.size());

            for(LoyaltyCard c : subscribedLoyaltyCards) {

                if(c.getId().equals(cafeId)) {

                    LoyaltyCard incrementedCard = new LoyaltyCard(c);
                    updatedPoints = incrementedCard.incrementPoints();
                    clonedList.add(incrementedCard);
                    result = incrementedCard;
                } else {
                    clonedList.add(c);
                }
            }
            mSubscribedCafes.setValue(clonedList);
        }

        // update the database if the card was successfully incremented
        if(result != null) {
            SQLiteDatabase db = mDbHelper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(DBHelper.CAFES_COLUMN_USERPOINTS, updatedPoints);
            db.update(DBHelper.TABLENAME_CAFES,
                    cv,
                    DBHelper.CAFES_COLUMN_ID + "=?",
                    new String[] { cafeId });
            db.close();
        }

        return result;

    }
}
