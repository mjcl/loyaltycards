package com.mjcl.loyaltycards.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mjcl.loyaltycards.LoyaltyCard;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/* SQLite Database helper class. Creates the SQLite DB and performs initial data population. */
public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "LoyaltyCards";
    private static final int DATABASE_VERSION = 1;

    // column names for table Cafes
    public static final String TABLENAME_CAFES = "cafes";
    public static final String CAFES_COLUMN_ID = "cafeId";
    public static final String CAFES_COLUMN_CAFENAME = "cafeName";
    public static final String CAFES_COLUMN_USERPOINTS = "userPoints";
    public static final String CAFES_COlUMN_SUBSCRIBED = "subscribed";
    public static final String CAFES_COLUMN_LOGO_ID = "logo";
    public static final String CAFES_COLUMN_LOCATION = "location";
    public static final String CAFES_COLUMN_HOURS = "hours";
    public static final String CAFES_COLUMN_FB_URL = "facebookUrl";
    public static final String CAFES_COLUMN_FB_ID = "facebookId";
    public static final String CAFES_COLUMN_MAPS_ID = "mapsId";

    public static final int SUBSCRIBED = 1;
    public static final int UNSUBSCRIBED = 0;
    public static final int ALLCARDS = -1;

    // initial list of cafes to populate db with
    private ArrayList<LoyaltyCard> mLoyaltyCards = new ArrayList<>();

    // reference to context needed to call getAssets()
    private Context context;
    private Resources resources;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
//        SQLiteDatabase db = getWritableDatabase();
//        db.execSQL("DROP TABLE IF EXISTS cafes");
//        Cursor csr = db.rawQuery("SELECT * FROM cafes", null);
//        csr.close();
        this.context = context;
        this.resources = context.getResources();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String createTable = "CREATE TABLE " + TABLENAME_CAFES + "(" +
                CAFES_COLUMN_ID + " TEXT PRIMARY KEY," +
                CAFES_COLUMN_CAFENAME + " TEXT," +
                CAFES_COLUMN_USERPOINTS + " INTEGER," +
                CAFES_COlUMN_SUBSCRIBED + " INTEGER," +
                CAFES_COLUMN_LOCATION + " TEXT," +
                CAFES_COLUMN_HOURS + " TEXT," +
                CAFES_COLUMN_FB_URL + " TEXT," +
                CAFES_COLUMN_FB_ID + " TEXT," +
                CAFES_COLUMN_MAPS_ID + " TEXT," +
                CAFES_COLUMN_LOGO_ID + " INTEGER)";
        db.execSQL(createTable);

        // fetch cafe list from assets folder
        parseCafesFromJSON();

        // populate DB with initial data
        for(int i = 0; i < mLoyaltyCards.size(); i++) {
            insertCafe(mLoyaltyCards.get(i), db);
        }
    }

    // takes a LoyaltyCard object and inserts and stores its attributes
    // in db
    private void insertCafe(LoyaltyCard loyaltyCard, SQLiteDatabase db) {

        ContentValues values = new ContentValues();
        values.put(CAFES_COLUMN_ID, loyaltyCard.getId());
        values.put(CAFES_COLUMN_CAFENAME, loyaltyCard.getName());
        values.put(CAFES_COLUMN_USERPOINTS, loyaltyCard.getPoints());
        values.put(CAFES_COlUMN_SUBSCRIBED, loyaltyCard.getSubscribed() ? SUBSCRIBED : UNSUBSCRIBED);
        values.put(CAFES_COLUMN_LOGO_ID, loyaltyCard.getLogoId());
        values.put(CAFES_COLUMN_LOCATION, loyaltyCard.getLocation());
        values.put(CAFES_COLUMN_HOURS, loyaltyCard.getHours());
        values.put(CAFES_COLUMN_FB_URL, loyaltyCard.getFacebookUrl());
        values.put(CAFES_COLUMN_FB_ID, loyaltyCard.getFacebookId());
        values.put(CAFES_COLUMN_MAPS_ID, loyaltyCard.getMapsId());

        db.insert(TABLENAME_CAFES, null, values);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    private void parseCafesFromJSON() {
        try {

            JSONObject obj = new JSONObject(loadJSONFromAsset("cafes.json"));
            JSONArray cafeArray = obj.getJSONArray("cafes");


            for(int i = 0; i < cafeArray.length(); i++) {

                JSONObject jsonCafe = cafeArray.getJSONObject(i);
                int logoId = resources.getIdentifier(jsonCafe.getString("image"),
                        "drawable", context.getPackageName());

                mLoyaltyCards.add(new LoyaltyCard(jsonCafe.getString("id"),
                                        jsonCafe.getString("name"),
                                        jsonCafe.getInt("points"),
                                        logoId,
                                        jsonCafe.getString("location"),
                                        jsonCafe.getString("hours"),
                                        jsonCafe.getString("facebook_url"),
                                        jsonCafe.getString("facebook_id"),
                                        jsonCafe.getString("map_id"),
                                        jsonCafe.getBoolean("subscribed")));
            }
        } catch(JSONException e) {
            e.printStackTrace();
        }
    }


    // taken from https://abhiandroid.com/programming/json
    public String loadJSONFromAsset(String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return json;
    }
}
