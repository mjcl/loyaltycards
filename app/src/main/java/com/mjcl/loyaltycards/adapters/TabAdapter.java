package com.mjcl.loyaltycards.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class TabAdapter extends FragmentPagerAdapter {
    private List<Fragment> fragments = new ArrayList<>(); //Fragment List
    private List<String> namePage = new ArrayList<>(); // Fragment Name List
    public TabAdapter(FragmentManager fm) {
        super(fm);
    }

    public void add(Fragment Frag, String Title) {
        fragments.add(Frag);
        namePage.add(Title);
    }
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }
    @Override
    public CharSequence getPageTitle(int position) {
        return namePage.get(position);
    }
    @Override
    public int getCount() {
        return fragments.size();
    }
}
