package com.mjcl.loyaltycards.adapters;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mjcl.loyaltycards.LoyaltyCard;
import com.mjcl.loyaltycards.R;

import java.util.List;

public class SubCardListAdapter extends RecyclerView.Adapter<SubCardListAdapter.ViewHolder> {

    private Context context;
    private List<LoyaltyCard> mLoyaltyCards;
    private OnCardListener mOnCardListener;


  public SubCardListAdapter(List<LoyaltyCard> loyaltyCards, OnCardListener onCardListener,
                            Context context) {
        this.context = context;
        mLoyaltyCards = loyaltyCards;
        mOnCardListener = onCardListener;
    }

    public void setList(List<LoyaltyCard> updatedList) {
      mLoyaltyCards = updatedList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.loyalty_card_subbed, viewGroup, false);
        return new ViewHolder(view, mOnCardListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        LoyaltyCard c = mLoyaltyCards.get(position);

        holder.cafeName.setText(c.getName());
        holder.cafeLogo.setImageResource(c.getLogoId());

        if(c.getPoints() == 5) {
            holder.freeCoffeeBanner.setVisibility(View.VISIBLE);
        } else {
            holder.freeCoffeeBanner.setVisibility(View.GONE);
        }

        // creating the 'progress bar'
        //todo get rid of magic number 5 (number of coffee stamps on card)
        for(int i = 0; i < 5; i++) {
            ImageView coffeeIcon = (ImageView) holder.coffee_progress.getChildAt(i);
            if(i < c.getPoints()) {
                // set to filled cup
                coffeeIcon.setImageResource(R.drawable.stain_heart);
            } else {
                coffeeIcon.setImageResource(R.drawable.stain_heart_grey);
            }
        }

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {


        if(payloads.isEmpty()) super.onBindViewHolder(holder, position, payloads);
        else {
            Bundle payload = (Bundle) payloads.get(0);
            // take appropriate action based on payload contents
            for(String key : payload.keySet()) {
                if(key.equals("points")) {

                    int newPoints = payload.getInt(key);
                    // reset progress
                    //todo get rid of magic number 5 (number of coffee stamps on card)
                    if(newPoints == 1) {

                        showFreeCoffeeDialogue(mLoyaltyCards.get(position));
                        holder.displayDialogOnClick = true;

                    }

                    if(newPoints == 5) {
                        holder.freeCoffeeBanner.setVisibility(View.VISIBLE);
                    } else {
                        holder.freeCoffeeBanner.setVisibility(View.GONE);
                    }

                    for(int i = 0; i < 5; i++) {
                        ImageView coffeeIcon = (ImageView) holder.coffee_progress.getChildAt(i);
                        if(i < newPoints) {
                            // set to filled cup
                            coffeeIcon.setImageResource(R.drawable.stain_heart);
                        } else {
                            coffeeIcon.setImageResource(R.drawable.stain_heart_grey);
                        }
                    }
                }
            }
        }
    }

    private void showFreeCoffeeDialogue(LoyaltyCard loyaltyCard) {

      final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);


      // dialog setup
      dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
      dialog.setContentView(R.layout.free_coffee_dialogue);
      dialog.setCanceledOnTouchOutside(false);
      dialog.setCancelable(true);

      // init views
      ViewGroup dialogBox = dialog.findViewById(R.id.dialogue_view);
      ImageView cafeLogo = dialog.findViewById(R.id.dialogue_cafe_logo);
      TextView messageView = dialog.findViewById(R.id.dialogue_message);

      cafeLogo.setImageResource(loyaltyCard.getLogoId());

      String message = context.getResources().getString(R.string.dialog_message) +
                        loyaltyCard.getName() + "!";
      messageView.setText(message);

      dialogBox.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              dialog.dismiss();
          }
      });
      dialog.show();

  }


    @Override
    public int getItemCount() {
        if(mLoyaltyCards != null) {
            return mLoyaltyCards.size();
        } else return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
        implements  View.OnLongClickListener, View.OnClickListener{

        TextView cafeName;
        ImageView cafeLogo;
        LinearLayout coffee_progress;
        TextView freeCoffeeBanner;
        OnCardListener onCardListener;
        boolean displayDialogOnClick = false;

        public ViewHolder(View view, OnCardListener onCardListener) {
            super(view);
            cafeName = view.findViewById(R.id.sub_card_cafe_name);
            cafeLogo = view.findViewById(R.id.sub_card_cafe_logo);
            coffee_progress = view.findViewById(R.id.sub_coffee_progress);
            freeCoffeeBanner = view.findViewById(R.id.card_free_coffee);

            this.onCardListener = onCardListener;


            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            // enable clicking after a delay.
            view.setClickable(false);
            view.setLongClickable(false);
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    itemView.setClickable(true);
                    itemView.setLongClickable(true);
                }
            }, 1000); //1 second delay.

        }

        @Override
        public void onClick(View view) {
            // clicking on the card will show the free coffee dialog only when the user
            // reached the required points in the same session. This is intended to prevent users
            // from 'reusing' the reward at a later time.
            if(displayDialogOnClick) {
                showFreeCoffeeDialogue(mLoyaltyCards.get(getAdapterPosition()));
            } else {
                onCardListener.onCardClick(getAdapterPosition());
            }

        }

        @Override
        public boolean onLongClick(final View view) {

            // fire a haptic event
            Vibrator vibrator = (Vibrator) view.getContext().getSystemService(Context.VIBRATOR_SERVICE);
            if (vibrator.hasVibrator()) {
                vibrator.vibrate(100); // for 200 ms
            }

            // prevent rapid fire clicking
            view.setClickable(false);
            view.setLongClickable(false);

            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    view.setClickable(true);
                    view.setLongClickable(true);
                }
            }, 1000); //1 second delay.

            onCardListener.onCardLongClick(getAdapterPosition());
            return false;
        }
    }

    public interface OnCardListener {

    void onCardClick(int position);

    void onCardLongClick(int position);
    }
}
