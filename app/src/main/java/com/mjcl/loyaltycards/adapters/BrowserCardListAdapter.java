package com.mjcl.loyaltycards.adapters;

import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mjcl.loyaltycards.LoyaltyCard;
import com.mjcl.loyaltycards.R;

import java.util.List;

public class BrowserCardListAdapter extends RecyclerView.Adapter<BrowserCardListAdapter.ViewHolder> {

    private List<LoyaltyCard> mLoyaltyCards;
    private OnCardListener mOnCardListener;

  public BrowserCardListAdapter(List<LoyaltyCard> loyaltyCards, OnCardListener onCardListener) {
        mLoyaltyCards = loyaltyCards;
        mOnCardListener = onCardListener;
    }

    public void setList(List<LoyaltyCard> updatedList) {
      mLoyaltyCards = updatedList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.loyalty_card_browser, viewGroup, false);
        return new ViewHolder(view, mOnCardListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        LoyaltyCard c = mLoyaltyCards.get(position);

        holder.cafeName.setText(c.getName());
        holder.cafeLogo.setImageResource(c.getLogoId());
        holder.cafeLocation.setText(c.getLocation());

        if(mLoyaltyCards.get(position).getSubscribed()) {
            holder.subIcon.setVisibility(View.VISIBLE);
        } else holder.subIcon.setVisibility(View.GONE);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {

        if(payloads.isEmpty()) super.onBindViewHolder(holder, position, payloads);
        else {
            Bundle payload = (Bundle) payloads.get(0);
            // take appropriate action based on payload contents
            for(String key : payload.keySet()) {
                if(key.equals("subscribed")) {

                    boolean subscribed = payload.getBoolean(key);

                    if(subscribed) holder.subIcon.setVisibility(View.VISIBLE);
                    else holder.subIcon.setVisibility(View.GONE);

                }
            }
        }
    }


    @Override
    public int getItemCount() {
        if(mLoyaltyCards != null) {
            return mLoyaltyCards.size();
        } else return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
        implements  View.OnLongClickListener, View.OnClickListener{



        TextView cafeName;
        TextView cafeLocation;
        ImageView cafeLogo;
        ImageView subIcon;


        OnCardListener onCardListener;

        public ViewHolder(View view, OnCardListener onCardListener) {
            super(view);
            cafeName = view.findViewById(R.id.browser_card_cafe_name);
            cafeLocation = view.findViewById(R.id.browser_card_location);
            cafeLogo = view.findViewById(R.id.browser_card_cafe_logo);
            subIcon = view.findViewById(R.id.browser_card_sub_icon);

            this.onCardListener = onCardListener;


            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            // enable clicking after a delay.
            view.setClickable(false);
            view.setLongClickable(false);
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    itemView.setClickable(true);
                    itemView.setLongClickable(true);
                }
            }, 1000); //1 second delay.

        }

        @Override
        public void onClick(View view) {
            onCardListener.onCardClick(getAdapterPosition());

        }

        @Override
        public boolean onLongClick(final View view) {

            // fire a haptic event
            Vibrator vibrator = (Vibrator) view.getContext().getSystemService(Context.VIBRATOR_SERVICE);
            if (vibrator.hasVibrator()) {
                vibrator.vibrate(100); // for 200 ms
            }

            // prevent rapid fire clicking.
            view.setClickable(false);
            view.setLongClickable(false);

            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    view.setClickable(true);
                    view.setLongClickable(true);
                }
            }, 1000); //1 second delay.

            onCardListener.onCardLongClick(getAdapterPosition());
            return false;
        }
    }

    public interface OnCardListener {

    void onCardClick(int position);

    void onCardLongClick(int position);
    }
}
