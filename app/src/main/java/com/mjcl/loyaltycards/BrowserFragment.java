package com.mjcl.loyaltycards;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mjcl.loyaltycards.adapters.BrowserCardListAdapter;
import com.mjcl.loyaltycards.adapters.SubCardListAdapter;
import com.mjcl.loyaltycards.viewmodels.LoyaltyCardsViewModel;

import java.util.List;

public class BrowserFragment extends Fragment
    implements BrowserCardListAdapter.OnCardListener{

    private List<LoyaltyCard> mLoyaltyCards;
    private LoyaltyCardsViewModel mViewModel;

    // UI components
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mCardRecyclerAdapter;

    // Fragment that displays the browser screen.
    public static BrowserFragment newInstance() {
        return new BrowserFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.browser_fragment, container, false);

        mRecyclerView = view.findViewById(R.id.browser_cards_recycler_view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // bind to viewmodel
        mViewModel = ViewModelProviders.of(getActivity()).get(LoyaltyCardsViewModel.class);

        // init recyclerview
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getActivity());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // observe viewmodel
        final Observer<List<LoyaltyCard>> cafesObserver = new Observer<List<LoyaltyCard>>() {

            @Override
            public void onChanged(final List<LoyaltyCard> updatedList) {

                if(mLoyaltyCards == null) {
                    mLoyaltyCards = updatedList;
                    mCardRecyclerAdapter = new BrowserCardListAdapter(mLoyaltyCards,
                            BrowserFragment.this);
                    mRecyclerView.setAdapter(mCardRecyclerAdapter);
                } else {
                    DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                        @Override
                        public int getOldListSize() {
                            return mLoyaltyCards.size();
                        }

                        @Override
                        public int getNewListSize() {
                            return updatedList.size();
                        }

                        @Override
                        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                            return mLoyaltyCards.get(oldItemPosition).getId() ==
                                    updatedList.get(newItemPosition).getId();
                        }

                        @Override
                        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                            LoyaltyCard oldLoyaltyCard = mLoyaltyCards.get(oldItemPosition);
                            LoyaltyCard newLoyaltyCard = updatedList.get(newItemPosition);
                            return oldLoyaltyCard.equals(newLoyaltyCard);
                        }

                        @Nullable
                        @Override
                        public Object getChangePayload(int oldItemPosition, int newItemPosition) {
                            LoyaltyCard oldLoyaltyCard = mLoyaltyCards.get(oldItemPosition);
                            LoyaltyCard newLoyaltyCard = updatedList.get(newItemPosition);

                            Bundle diff = new Bundle();

                            if(oldLoyaltyCard.getSubscribed() != newLoyaltyCard.getSubscribed()) {
                                diff.putBoolean("subscribed", newLoyaltyCard.getSubscribed());
                            }
                            if(diff.size() == 0) {
                                // Currently this will never happen because a change in subscription
                                // is the only thing that will cause getChangePayload to be called.
                                return null;
                            }

                            return diff;
                        }
                    });

                    ((BrowserCardListAdapter) mCardRecyclerAdapter).setList(updatedList);
                    result.dispatchUpdatesTo(mCardRecyclerAdapter);
                    mLoyaltyCards = updatedList;
                }
            }
        };

        mViewModel.getBrowserCafes().observe(this, cafesObserver);
    }

    // go to card detail screen
    @Override
    public void onCardClick(int position) {
        Intent intent = new Intent(getContext(), CardDetailActivity.class);
        intent.putExtra("selectedCard", mLoyaltyCards.get(position));
        startActivity(intent);

    }

    // subscribe
    @Override
    public void onCardLongClick(int position) {
        String cardId = mLoyaltyCards.get(position).getId();
        if(mViewModel.subscribeToCard(cardId)) {

            String text = "Subscribed to " + mLoyaltyCards.get(position).getName();
            Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
        } else {
            String text = "You're already subscribed!";
            Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
        }
    }
}
