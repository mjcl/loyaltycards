package com.mjcl.loyaltycards;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mjcl.loyaltycards.utils.UrlHelper;

import java.util.List;

public class CardDetailActivity extends AppCompatActivity
    implements View.OnClickListener {

    // UI
    ImageView mCafeLogo;
    TextView mCafeName;
    TextView mLocation;
    TextView mHours;
    ImageView mFacebookLink;
    ImageView mMapsLink;

    LoyaltyCard card;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_detail);

        if(getIntent().hasExtra("selectedCard")) {

            card = getIntent().getParcelableExtra("selectedCard");

            mCafeLogo = findViewById(R.id.detail_cafe_logo);
            mCafeName = findViewById(R.id.detail_cafe_name);
            mLocation = findViewById(R.id.detail_location);
            mHours = findViewById(R.id.detail_hours);
            mFacebookLink = findViewById(R.id.detail_facebook_link);
            mMapsLink = findViewById(R.id.detail_maps_link);


            mCafeLogo.setImageResource(card.getLogoId());
            mCafeName.setText(card.getName());
            mLocation.setText(card.getLocation());
            mHours.setText(card.getHours());

            mFacebookLink.setOnClickListener(this);
            mMapsLink.setOnClickListener(this);


        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId())
        {
            case R.id.detail_facebook_link:

                // Display a toast if no facebook page exists.
                if(card.getFacebookUrl().equals("none")) {

                    Context context = getApplicationContext();
                    CharSequence text = "These guys don't have a facebook page! (Yet...)";
                    Toast.makeText(context, text, Toast.LENGTH_SHORT).show();

                } else { // otherwise create an intent directing to the facebook page.

                    String text = UrlHelper.getFaceBookUrl(getPackageManager(),
                            card.getFacebookUrl(), card.getFacebookId());

                    Uri fbPageUri = Uri.parse(text);
                    Intent intent = new Intent(Intent.ACTION_VIEW, fbPageUri);
                    // verify it resolves (i.e. facebook is installed on the device).
                    // todo create intent to regular url
                    PackageManager packageManager = getPackageManager();
                    List<ResolveInfo> activities = packageManager.queryIntentActivities(intent,
                            PackageManager.MATCH_DEFAULT_ONLY);
                    boolean isIntentSafe = activities.size() > 0;
                    // start the intent
                    if (isIntentSafe) {
                        startActivity(intent);
                    }
                } break;

            case R.id.detail_maps_link:

                String text = UrlHelper.getGMapsUrl(card.getName(), card.getMapsId());

                Uri mapsUri = Uri.parse(text);
                Intent intent = new Intent(Intent.ACTION_VIEW, mapsUri);

                // start intent if safe
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                } break;

        }
    }
}
