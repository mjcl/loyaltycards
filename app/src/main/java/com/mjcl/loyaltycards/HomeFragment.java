package com.mjcl.loyaltycards;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mjcl.loyaltycards.adapters.SubCardListAdapter;
import com.mjcl.loyaltycards.viewmodels.LoyaltyCardsViewModel;

import java.util.List;

// View for the main screen containing the user's subscribed loyalty cards.
// Contains loyalty cards in a RecyclerView list.
public class HomeFragment extends Fragment
    implements SubCardListAdapter.OnCardListener{

    // subscribed loyalty cards
    private List<LoyaltyCard> mLoyaltyCards;

    private LoyaltyCardsViewModel mViewModel;

    // UI components
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mCardRecyclerAdapter;
    private TextView mEmptyMessage;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);

        mRecyclerView = view.findViewById(R.id.home_cards_recycler_view);
        mEmptyMessage = view.findViewById(R.id.home_empty_message);

        return view;

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // bind to viewmodel
        mViewModel = ViewModelProviders.of(getActivity()).get(LoyaltyCardsViewModel.class);

        //init recyclerview
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getActivity());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // observe viewmodel
        final Observer<List<LoyaltyCard>> cafesObserver = new Observer<List<LoyaltyCard>>() {

            @Override
            public void onChanged(final List<LoyaltyCard> updatedList) {

                // case where RecyclerView gets populated for the first time.
                if(mLoyaltyCards == null) {
                    mLoyaltyCards = updatedList;
                    mCardRecyclerAdapter = new SubCardListAdapter(mLoyaltyCards,
                            HomeFragment.this,
                            getContext());
                    mRecyclerView.setAdapter(mCardRecyclerAdapter);

                    // Hide the empty message if there are items in the list.
                    if(mLoyaltyCards.isEmpty()) {
                        mEmptyMessage.setVisibility(View.VISIBLE);
                    }
                } else {
                    // DiffUtil is used to efficiently updated the RecyclerView.
                    DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                        @Override
                        public int getOldListSize() {
                            return mLoyaltyCards.size();
                        }

                        @Override
                        public int getNewListSize() {
                            return updatedList.size();
                        }

                        @Override
                        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {

                            return mLoyaltyCards.get(oldItemPosition).getId().equals(
                                    updatedList.get(newItemPosition).getId());
                        }

                        @Override
                        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                            LoyaltyCard oldLoyaltyCard = mLoyaltyCards.get(oldItemPosition);
                            LoyaltyCard newLoyaltyCard = updatedList.get(newItemPosition);
                            return oldLoyaltyCard.equals(newLoyaltyCard);
                        }

                        @Nullable
                        @Override
                        public Object getChangePayload(int oldItemPosition, int newItemPosition) {
                            LoyaltyCard oldLoyaltyCard = mLoyaltyCards.get(oldItemPosition);
                            LoyaltyCard newLoyaltyCard = updatedList.get(newItemPosition);

                            Bundle diff = new Bundle();

                            // Number of loyalty cards is unchanged but points are updated.
                            if(newLoyaltyCard.getPoints() != oldLoyaltyCard.getPoints()) {
                                diff.putInt("points", newLoyaltyCard.getPoints());
                            }
                            if(diff.size() == 0) {
                                // Currently this will never happen because a change in points
                                // is the only thing that will cause getChangePayload to be called.
                                return null;
                            }

                            return diff;
                        }
                    });

                    // Send the diff to the RecyclerView adapter.
                    ((SubCardListAdapter) mCardRecyclerAdapter).setList(updatedList);
                    result.dispatchUpdatesTo(mCardRecyclerAdapter);

                    // update the Fragments copy of the list.
                    mLoyaltyCards = updatedList;

                    // display/hide the 'empty' message depending on the count.
                    if(updatedList.isEmpty()) {
                        mEmptyMessage.setVisibility(View.VISIBLE);
                    } else {
                        mEmptyMessage.setVisibility(View.GONE);
                    }
                }
            }
        };

        mViewModel.getSubscribedCafes().observe(getViewLifecycleOwner(), cafesObserver);

    }

    //go to card detail screen
    @Override
    public void onCardClick(int position) {
        Intent intent = new Intent(getContext(), CardDetailActivity.class);
        intent.putExtra("selectedCard", mLoyaltyCards.get(position));

        startActivity(intent);
    }

    // unsubscribe
    @Override
    public void onCardLongClick(int position) {
        String cardId = mLoyaltyCards.get(position).getId();
        mViewModel.unsubscribeToCard(cardId);

    }
}
