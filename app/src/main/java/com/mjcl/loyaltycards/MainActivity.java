package com.mjcl.loyaltycards;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.mjcl.loyaltycards.adapters.TabAdapter;
import com.mjcl.loyaltycards.viewmodels.LoyaltyCardsViewModel;


public class MainActivity extends FragmentActivity {

    public static final int NFC_SCAN_DELAY = 10000; // milliseconds

    private ViewPager viewPager;
    private BottomNavigationView navigation;

    private LoyaltyCardsViewModel mViewModel;

    // Used to limit frequency with which tags can be scanned. See incrementCard()
    Handler nfcDelayHandler = new Handler();
    private boolean availableToScan = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // bind to viewmodel. NFC tag data will be passed to viewmodel, so that fragments/cards
        // can be updated.
        mViewModel = ViewModelProviders.of(this).get(LoyaltyCardsViewModel.class);

        // set up navigation bar
        navigation = findViewById(R.id.navigation);
        // set up listener for navigation. The listener will change the
        // displayed fragment in the viewpager.
        navigation.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch(menuItem.getItemId()) {
                            case R.id.navigation_home:
                                viewPager.setCurrentItem(0);
                                return true;
                            case R.id.navigation_dashboard:
                                viewPager.setCurrentItem(1);
                                return true;
                        }
                        return false;
                    }
                }
        );

        // set up viewpager
        viewPager = findViewById(R.id.viewpager);
        addFragmentsToPager(getSupportFragmentManager(), viewPager);

        // set viewpager to home when activity starts
        viewPager.setCurrentItem(0);

        // set listener for changes in the viewpager. The listener will change the highlighted
        // selected icon in the navbar. (For when the user swipes to change view?)
        viewPager.addOnPageChangeListener(new PageChange());

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        // Retrieve the tag from the NFC intent that started the activity.
        if(availableToScan) {
            if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction()) ||
                    NfcAdapter.ACTION_TECH_DISCOVERED.equals(intent.getAction())) {
                // todo check nfc tag contains string data
                byte[] uidBytes = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
                String uid = bytesToHex(uidBytes);
                incrementCard(uid);
            }
        }
    }

    private void incrementCard(String uid) {

        LoyaltyCard card = mViewModel.incrementCard(uid);

        if(card == null){
            Toast.makeText(this, "Add this cafe to your subscriptions first!",
                    Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(this, "You tagged " + card.getName() + "!",
                    Toast.LENGTH_SHORT).show();
        }

        availableToScan = false;
        nfcDelayHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                availableToScan = true;
            }
        }, NFC_SCAN_DELAY);
    }

    // taken from https://stackoverflow.com/questions/9655181/how-to-convert-a-byte-array-to-a-hex-string-in-java
    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    private class PageChange implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

            switch(position) {
                case 0:
                    navigation.setSelectedItemId(R.id.navigation_home);
                    break;
                case 1:
                    navigation.setSelectedItemId(R.id.navigation_dashboard);
                    break;
            }

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    // helper function for setting up viewpager
    private static void addFragmentsToPager(FragmentManager fm, ViewPager vp) {

        TabAdapter tabAdapter = new TabAdapter(fm);

        // do we need titles?
        tabAdapter.add(HomeFragment.newInstance(), "Home");
        tabAdapter.add(BrowserFragment.newInstance(), "Cafes on Campus");

        vp.setAdapter(tabAdapter);

    }

}
